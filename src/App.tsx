import { Route, Routes } from "react-router-dom";
import "./scss/index.scss";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AuthLayout from "./components/layouts/auth/AuthLayout";
import SignInPage from "./components/pages/sign-in/SignInPage";
import SignUpPage from "./components/pages/sign-up/SignUpPage";
import ForgotPasswordPage from "./components/pages/forgot-password/ForgotPasswordPage";
import ResetPasswordPage from "./components/pages/reset-password/Reset-PasswordPage";
import AuthProvider from "./components/layouts/auth-provider/AuthProvider";
import DashboardLayout from "./components/layouts/dashboard/DashboardLayout";
import LectorsPage from "./components/pages/lectors/LectorsPage";
import CoursesPage from "./components/pages/courses/CoursesPage";
import StudentsPage from "./components/pages/students/StudentsPage";
import EditStudentPage from "./components/pages/edit-student/EditStudentPage";
import GroupsPage from "./components/pages/groups/GroupsPage";

function App() {
    return (
        <>
            <Routes>
                <Route element={<AuthLayout />}>
                    <Route path="/" element={<SignInPage />} />
                    <Route path="/sign-up" element={<SignUpPage />} />
                    <Route path="/forgot-password" element={<ForgotPasswordPage />} />
                    <Route path="/reset-password" element={<ResetPasswordPage />} />
                </Route>
                <Route element={<AuthProvider />}>
                    <Route element={<DashboardLayout />}>
                        <Route path="/courses" element={<CoursesPage />} />
                        <Route path="/lectors" element={<LectorsPage />} />
                        <Route path="/groups" element={<GroupsPage />} />
                        <Route path="/students" element={<StudentsPage />} />
                    </Route>
                    <Route path="/students/:id" element={<EditStudentPage />} />
                </Route>
            </Routes>
            <ToastContainer position="bottom-right" />
        </>
    );
}
export default App;
