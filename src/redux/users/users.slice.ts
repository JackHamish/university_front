import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    user: {},
    loading: false,
    error: null,
};

const usersSlice = createSlice({
    name: "users",
    initialState: initialState,
    reducers: {},
});

export const {} = usersSlice.actions;

export default usersSlice.reducer;
