import { createAsyncThunk } from "@reduxjs/toolkit";

import { IGroupCreate, createGroupApi, getGroupsApi, updateGroupApi } from "../../api/groups.api";

export const getGroupsReq = createAsyncThunk(
    "/groupsGet",
    async (
        params: {
            name?: string;
            sortField?: string;
            sortOrder?: string;
            limit?: number;
            offset?: number;
        },
        { rejectWithValue }
    ) => {
        try {
            const response = await getGroupsApi(params);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const createGroupsReq = createAsyncThunk(
    "/groupsCreate",
    async (data: IGroupCreate, { rejectWithValue }) => {
        try {
            const response = await createGroupApi(data);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const updateGroupsReq = createAsyncThunk(
    "/groupsUpdate",
    async ({ id, data }: { id: string; data: Partial<IGroupCreate> }, { rejectWithValue }) => {
        try {
            const response = await updateGroupApi(id, data);

            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);
