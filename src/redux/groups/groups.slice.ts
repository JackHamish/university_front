import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ICourse } from "../../api/course.api";
import { createGroupsReq, getGroupsReq, updateGroupsReq } from "./action.creators";
import { IGroup } from "../../api/groups.api";

interface IGroupsState {
    groups: IGroup[];
    itemsCount: number;
    loading: boolean;
    error: boolean;
    success: boolean;
}

const initialState: IGroupsState = {
    groups: [],
    itemsCount: 0,
    loading: false,
    error: false,
    success: false,
};

const groupsSlice = createSlice({
    name: "courses",
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getGroupsReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(
                getGroupsReq.fulfilled,
                (
                    state,
                    action: PayloadAction<{ data: ICourse[]; meta: { itemCount: number } }>
                ) => {
                    state.loading = false;
                    state.success = true;
                    state.groups = action.payload.data;
                    state.itemsCount = action.payload.meta.itemCount;
                }
            )
            .addCase(getGroupsReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
                state.itemsCount = 0;
                state.groups = [];
            })
            .addCase(createGroupsReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(createGroupsReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(createGroupsReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            })
            .addCase(updateGroupsReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(updateGroupsReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(updateGroupsReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            });
    },
});

export const {} = groupsSlice.actions;

export default groupsSlice.reducer;
