import { createAsyncThunk } from "@reduxjs/toolkit";
import { loginApi, registerApi } from "../../api/auth.api";
import { getMeApi } from "../../api/user.api";
import {
    ILector,
    ILectorCreate,
    createLectorApi,
    getLectorsApi,
    updateLectorApi,
} from "../../api/lector.api";
import {
    IStudentCreate,
    IStudentUpdate,
    createStudentApi,
    getStudentByIdApi,
    getStudentsApi,
    updateStudentApi,
} from "../../api/student.api";

export const getStudentsReq = createAsyncThunk(
    "/studentsGet",
    async (
        params: {
            name?: string;
            sortField?: string;
            sortOrder?: string;
            limit?: number;
            offset?: number;
        },
        { rejectWithValue }
    ) => {
        try {
            const response = await getStudentsApi(params);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const getStudentByIdReq = createAsyncThunk(
    "/studentGetById",
    async (id: string, { rejectWithValue }) => {
        try {
            const response = await getStudentByIdApi(id);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const createStudentReq = createAsyncThunk(
    "/studentsCreate",
    async (data: IStudentCreate, { rejectWithValue }) => {
        try {
            const response = await createStudentApi(data);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const updateStudentReq = createAsyncThunk(
    "/studentsUpdate",
    async ({ id, data }: { id: string; data: IStudentUpdate }, { rejectWithValue }) => {
        try {
            const response = await updateStudentApi(id, data);

            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);
