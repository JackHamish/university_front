import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IStudent } from "../../api/student.api";
import {
    createStudentReq,
    getStudentByIdReq,
    getStudentsReq,
    updateStudentReq,
} from "./actions.creators";
// import { createLectorReq, getLectorsReq, updateLectorReq } from "./actions.creators";
// import { ILector } from "../../api/lector.api";

interface IStudentsState {
    students: IStudent[];
    student: IStudent;
    itemsCount: number;
    loading: boolean;
    error: boolean;
    success: boolean;
}

const initialState: IStudentsState = {
    students: [],
    student: {} as IStudent,
    itemsCount: 0,
    loading: false,
    error: false,
    success: false,
};

const studentsSlice = createSlice({
    name: "students",
    initialState: initialState,
    reducers: {
        clearStudent(state) {
            state.student = {} as IStudent;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getStudentsReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(
                getStudentsReq.fulfilled,
                (
                    state,
                    action: PayloadAction<{ data: IStudent[]; meta: { itemCount: number } }>
                ) => {
                    state.loading = false;
                    state.success = true;
                    state.students = action.payload.data;
                    state.itemsCount = action.payload.meta.itemCount;
                }
            )
            .addCase(getStudentsReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
                state.itemsCount = 0;
                state.students = [];
            })
            .addCase(getStudentByIdReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(getStudentByIdReq.fulfilled, (state, action: PayloadAction<IStudent>) => {
                state.loading = false;
                state.success = true;
                state.student = action.payload;
            })
            .addCase(getStudentByIdReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
                state.student = {} as IStudent;
            })
            .addCase(createStudentReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(createStudentReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(createStudentReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            })
            .addCase(updateStudentReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(updateStudentReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(updateStudentReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            });
    },
});

export const { clearStudent } = studentsSlice.actions;

export default studentsSlice.reducer;
