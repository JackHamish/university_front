import { createAsyncThunk } from "@reduxjs/toolkit";
import { forgotPasswordApi, loginApi, registerApi, resetPasswordApi } from "../../api/auth.api";
import { getMeApi } from "../../api/user.api";
import { AxiosError } from "axios";

export const registerUserReq = createAsyncThunk(
    "/register",
    async ({ email, password }: { email: string; password: string }, { rejectWithValue }) => {
        try {
            const response = await registerApi(email, password);
            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const loginUserReq = createAsyncThunk(
    "/login",
    async ({ email, password }: { email: string; password: string }, { rejectWithValue }) => {
        try {
            const { data } = await loginApi(email, password);

            localStorage.setItem("userToken", data.accessToken);

            return data;
        } catch (err) {
            const error = err as Error | AxiosError;
            return rejectWithValue(error);
        }
    }
);

export const forgotPasswordReq = createAsyncThunk(
    "/forgotPassword",
    async ({ email }: { email: string }, { rejectWithValue }) => {
        try {
            const response = await forgotPasswordApi(email);

            return response;
        } catch (err) {
            const error = err as Error | AxiosError;
            return rejectWithValue(error);
        }
    }
);

export const resetPasswordReq = createAsyncThunk(
    "/resetPassword",
    async ({ token, password }: { token: string; password: string }, { rejectWithValue }) => {
        try {
            const response = await resetPasswordApi(token, password);

            return response;
        } catch (err) {
            const error = err as Error | AxiosError;
            return rejectWithValue(error);
        }
    }
);

export const getUserReq = createAsyncThunk("/me", async (thunkApi, { rejectWithValue }) => {
    try {
        const { data } = await getMeApi();

        return data;
    } catch (err) {
        const error = err as Error | AxiosError;
        return rejectWithValue(error);
    }
});
