import { createSlice } from "@reduxjs/toolkit";
import {
    forgotPasswordReq,
    getUserReq,
    loginUserReq,
    registerUserReq,
    resetPasswordReq,
} from "./action.creators";

const initialState = {
    user: {},
    loading: false,
    error: null,
    isLogged: false,
    success: false,
    resetSuccess: false,
};

const authSlice = createSlice({
    name: "auth",
    initialState: initialState,
    reducers: {
        logOut(state) {
            state.isLogged = false;
            state.success = false;
            state.user = {};
        },
        setSuccess(state) {
            state.success = false;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(registerUserReq.pending, (state, { payload }) => {
                state.loading = true;
                state.error = null;
                state.success = false;
            })
            .addCase(registerUserReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(registerUserReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
            });
        builder
            .addCase(loginUserReq.pending, (state, { payload }) => {
                state.loading = true;
                state.error = null;
                state.isLogged = false;
            })
            .addCase(loginUserReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.isLogged = true;
            })
            .addCase(loginUserReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.isLogged = false;
            });
        builder
            .addCase(getUserReq.pending, (state, { payload }) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(getUserReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.user = payload;
            })
            .addCase(getUserReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.isLogged = false;
                state.user = {};
            })

            .addCase(forgotPasswordReq.pending, (state, { payload }) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(forgotPasswordReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(forgotPasswordReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
            })
            .addCase(resetPasswordReq.pending, (state, { payload }) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(resetPasswordReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(resetPasswordReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
            });
    },
});

export const { logOut, setSuccess } = authSlice.actions;

export default authSlice.reducer;
