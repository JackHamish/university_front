import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ICourse } from "../../api/course.api";
import { createCourseReq, getCoursesReq, updateCourseReq } from "./actions.creator";

interface ICoursesState {
    courses: ICourse[];
    itemsCount: number;
    loading: boolean;
    error: boolean;
    success: boolean;
}

const initialState: ICoursesState = {
    courses: [],
    itemsCount: 0,
    loading: false,
    error: false,
    success: false,
};

const coursesSlice = createSlice({
    name: "courses",
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getCoursesReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(
                getCoursesReq.fulfilled,
                (
                    state,
                    action: PayloadAction<{ data: ICourse[]; meta: { itemCount: number } }>
                ) => {
                    state.loading = false;
                    state.success = true;
                    state.courses = action.payload.data;
                    state.itemsCount = action.payload.meta.itemCount;
                }
            )
            .addCase(getCoursesReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
                state.itemsCount = 0;
                state.courses = [];
            })
            .addCase(createCourseReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(createCourseReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(createCourseReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            })
            .addCase(updateCourseReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(updateCourseReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(updateCourseReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            });
    },
});

export const {} = coursesSlice.actions;

export default coursesSlice.reducer;
