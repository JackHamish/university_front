import { createAsyncThunk } from "@reduxjs/toolkit";
import {
    ICourseCreate,
    createCourseApi,
    getCoursesApi,
    updateCourseApi,
} from "../../api/course.api";

export const getCoursesReq = createAsyncThunk(
    "/coursesGet",
    async (
        params: {
            name?: string;
            sortField?: string;
            sortOrder?: string;
            limit?: number;
            offset?: number;
        },
        { rejectWithValue }
    ) => {
        try {
            const response = await getCoursesApi(params);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const createCourseReq = createAsyncThunk(
    "/coursesCreate",
    async (data: ICourseCreate, { rejectWithValue }) => {
        try {
            const response = await createCourseApi(data);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const updateCourseReq = createAsyncThunk(
    "/coursesUpdate",
    async ({ id, data }: { id: string; data: Partial<ICourseCreate> }, { rejectWithValue }) => {
        try {
            if (data.hours) data.hours = +data.hours;
            const response = await updateCourseApi(id, data);

            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);
