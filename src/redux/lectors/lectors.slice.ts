import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { createLectorReq, getLectorsReq, updateLectorReq } from "./actions.creators";
import { ILector } from "../../api/lector.api";

interface ILectorsState {
    lectors: ILector[];
    itemsCount: number;
    loading: boolean;
    error: boolean;
    success: boolean;
}

const initialState: ILectorsState = {
    lectors: [],
    itemsCount: 0,
    loading: false,
    error: false,
    success: false,
};

const lectorsSlice = createSlice({
    name: "lectors",
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getLectorsReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(
                getLectorsReq.fulfilled,
                (
                    state,
                    action: PayloadAction<{ data: ILector[]; meta: { itemCount: number } }>
                ) => {
                    state.loading = false;
                    state.success = true;
                    state.lectors = action.payload.data;
                    state.itemsCount = action.payload.meta.itemCount;
                }
            )
            .addCase(getLectorsReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
                state.itemsCount = 0;
                state.lectors = [];
            })
            .addCase(createLectorReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(createLectorReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(createLectorReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            })
            .addCase(updateLectorReq.pending, (state, { payload }) => {
                state.loading = true;
                state.success = false;
            })
            .addCase(updateLectorReq.fulfilled, (state, { payload }) => {
                state.loading = false;
                state.success = true;
            })
            .addCase(updateLectorReq.rejected, (state, { payload }) => {
                state.loading = false;
                state.success = false;
                state.error = true;
            });
    },
});

export const {} = lectorsSlice.actions;

export default lectorsSlice.reducer;
