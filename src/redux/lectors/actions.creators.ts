import { createAsyncThunk } from "@reduxjs/toolkit";
import { loginApi, registerApi } from "../../api/auth.api";
import { getMeApi } from "../../api/user.api";
import {
    ILector,
    ILectorCreate,
    createLectorApi,
    getLectorsApi,
    updateLectorApi,
} from "../../api/lector.api";

export const getLectorsReq = createAsyncThunk(
    "/lectorsGet",
    async (
        params: {
            name?: string;
            sortField?: string;
            sortOrder?: string;
            limit?: number;
            offset?: number;
        },
        { rejectWithValue }
    ) => {
        try {
            const response = await getLectorsApi(params);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const createLectorReq = createAsyncThunk(
    "/lectorsCreate",
    async (data: ILectorCreate, { rejectWithValue }) => {
        try {
            const response = await createLectorApi(data);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const updateLectorReq = createAsyncThunk(
    "/lectorsUpdate",
    async ({ id, data }: { id: string; data: Partial<ILectorCreate> }, { rejectWithValue }) => {
        try {
            const response = await updateLectorApi(id, data);

            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);
