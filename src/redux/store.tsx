import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./auth/auth.slice";
import usersSlice from "./users/users.slice";
import lectorsSlice from "./lectors/lectors.slice";
import studentsSlice from "./students/students.slice";
import coursesSlice from "./courses/courses.slice";
import groupsSlice from "./groups/groups.slice";

export const store = configureStore({
    reducer: {
        auth: authSlice,
        users: usersSlice,
        lectors: lectorsSlice,
        students: studentsSlice,
        courses: coursesSlice,
        groups: groupsSlice,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
