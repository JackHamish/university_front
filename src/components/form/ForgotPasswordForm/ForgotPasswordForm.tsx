import Button from "../../common/button/Button";
import Input from "../../common/input/Input";
import styles from "./ForgotPasswordForm.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

type ForgotPasswordFormFields = {
    email: string;
};

type ForgotPasswordFormProps = {
    onSubmit: SubmitHandler<ForgotPasswordFormFields>;
};

const ForgotPasswordForm = ({ onSubmit }: ForgotPasswordFormProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<ForgotPasswordFormFields>();

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="email"
                label="Email"
                placeholder="name@mail.com"
                name="email"
                register={register("email", {
                    required: true,
                    pattern:
                        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
            />
            {errors.email && <p className={styles.error}>Please check the Email</p>}

            <Button type="submit" label="Reset" />
        </form>
    );
};

export default ForgotPasswordForm;
