import { ChangeEvent, useState } from "react";
import Button from "../../common/button/Button";
import CheckBox from "../../common/check-box/CheckBox";
import Input from "../../common/input/Input";
import styles from "./ResetPasswordForm.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

type ResetPasswordFormFields = {
    password: string;
    confirmPassword: string;
};

type RegisterFormProps = {
    onSubmit: SubmitHandler<ResetPasswordFormFields>;
};

const ResetPasswordForm = ({ onSubmit }: RegisterFormProps) => {
    const [showPassword, setShowPassword] = useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<ResetPasswordFormFields>();

    const handleShowPassword = (event: ChangeEvent<HTMLInputElement>) => {
        setShowPassword(event.target.checked);
    };

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Input
                type={showPassword ? "text" : "password"}
                label="Password"
                placeholder=""
                name="password"
                register={register("password", {
                    required: true,
                    pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{6,15}$/,
                })}
            />
            {errors.password && <p className={styles.error}>Password is incorrect</p>}

            <Input
                type={showPassword ? "text" : "password"}
                label="Confirm Password"
                name="Confirm Password"
                placeholder=""
                register={register("confirmPassword", {
                    required: true,
                    pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{6,15}$/,
                })}
            />
            {errors.confirmPassword && <p className={styles.error}>Password is incorrect</p>}

            <CheckBox label="Show Password" checked={showPassword} onChange={handleShowPassword} />

            <Button type="submit" label="Reset" />
        </form>
    );
};

export default ResetPasswordForm;
