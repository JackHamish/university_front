import { ChangeEvent, useState } from "react";
import Button from "../../common/button/Button";
import Input from "../../common/input/Input";
import styles from "./CourseEditOrAddForm.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

export type CourseEditOrAddFormFields = {
    name: string;
    description: string;
    hours: number;
    [key: string]: string | number;
};

type CourseEditOrAddFormFProps = {
    onSubmit: SubmitHandler<CourseEditOrAddFormFields>;
    defaultValues?: Record<string, string | number>;
};

const CourseEditOrAddForm = ({ onSubmit, defaultValues }: CourseEditOrAddFormFProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<CourseEditOrAddFormFields>();

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="text"
                label="Name"
                name="name"
                register={register("name", {
                    required: true,
                    value: (defaultValues && (defaultValues["name"] as string)) ?? "",
                })}
            />
            {errors.name && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="text"
                label="Description"
                name="description"
                register={register("description", {
                    required: true,
                    maxLength: 50,
                    value: (defaultValues && (defaultValues["description"] as string)) || "",
                })}
            />
            {errors.description && <p className={styles.error}>description is incorrect</p>}

            <Input
                type="number"
                label="Hours"
                name="hours"
                register={register("hours", {
                    required: true,
                    value: (defaultValues && (defaultValues["hours"] as number)) || undefined,
                })}
            />
            {errors.hours && <p className={styles.error}>hours is incorrect</p>}

            <Button type="submit" label="Save" />
        </form>
    );
};

export default CourseEditOrAddForm;
