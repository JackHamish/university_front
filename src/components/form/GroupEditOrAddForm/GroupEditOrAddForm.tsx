import { ChangeEvent, useState } from "react";
import Button from "../../common/button/Button";
import Input from "../../common/input/Input";
import styles from "./GroupEditOrAddForm.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

export type GroupEditOrAddFormFields = {
    name: string;
    [key: string]: string;
};

type GroupEditOrAddFormFProps = {
    onSubmit: SubmitHandler<GroupEditOrAddFormFields>;
    defaultValues?: Record<string, string | number>;
};

const GroupEditOrAddForm = ({ onSubmit, defaultValues }: GroupEditOrAddFormFProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<GroupEditOrAddFormFields>();

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="text"
                label="Name"
                name="name"
                register={register("name", {
                    required: true,
                    value: (defaultValues && (defaultValues["name"] as string)) ?? "",
                })}
            />
            {errors.name && <p className={styles.error}>Name is incorrect</p>}

            <Button type="submit" label="Save" />
        </form>
    );
};

export default GroupEditOrAddForm;
