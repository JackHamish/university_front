import { ChangeEvent, useState } from "react";
import Button from "../../../common/button/Button";
import Input from "../../../common/input/Input";
import styles from "./CreateLectorFrom.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

export type CreateLectorFormFields = {
    name: string;
    surname: string;
    email: string;
    password: string;
};

type CreateLectorFormProps = {
    onSubmit: SubmitHandler<CreateLectorFormFields>;
};

const CreateLectorForm = ({ onSubmit }: CreateLectorFormProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<CreateLectorFormFields>();

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="text"
                label="Name"
                name="name"
                register={register("name", {
                    required: true,
                })}
            />
            {errors.name && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="text"
                label="Surname"
                name="surname"
                register={register("surname", {
                    required: true,
                })}
            />
            {errors.surname && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="email"
                label="Email"
                placeholder="name@mail.com"
                name="email"
                register={register("email", {
                    required: true,
                    pattern:
                        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
            />
            {errors.email && <p className={styles.error}>Email is incorrect</p>}

            <Input
                type={"text"}
                label="Password"
                placeholder=""
                name="password"
                register={register("password", {
                    required: true,
                    pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{6,15}$/,
                })}
            />
            {errors.password && <p className={styles.error}>Password is incorrect</p>}

            <Button type="submit" label="Create" />
        </form>
    );
};

export default CreateLectorForm;
