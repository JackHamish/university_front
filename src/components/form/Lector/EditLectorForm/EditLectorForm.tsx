import { ChangeEvent, useState } from "react";
import Button from "../../../common/button/Button";
import Input from "../../../common/input/Input";
import styles from "./EditLectorFrom.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

export type EditLectorFormFields = {
    name: string;
    surname: string;
    email: string;
    password: string;
    [key: string]: string;
};

type EditLectorFormProps = {
    onSubmit: SubmitHandler<EditLectorFormFields>;
    defaultValues: Record<string, string | number>;
};

const EditLectorForm = ({ onSubmit, defaultValues }: EditLectorFormProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<EditLectorFormFields>();

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="text"
                label="Name"
                name="name"
                register={register("name", {
                    required: false,
                    value: (defaultValues["name"] as string) || "",
                })}
            />
            {errors.name && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="text"
                label="Surname"
                name="surname"
                register={register("surname", {
                    required: false,
                    value: (defaultValues["surname"] as string) || "",
                })}
            />
            {errors.surname && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="email"
                label="Email"
                placeholder="name@mail.com"
                name="email"
                register={register("email", {
                    value: (defaultValues["email"] as string) || "",
                    required: false,
                    pattern:
                        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
            />
            {errors.email && <p className={styles.error}>Email is incorrect</p>}

            <Input
                type={"text"}
                label="Password"
                placeholder=""
                name="password"
                register={register("password", {
                    required: false,
                    pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{6,15}$/,
                })}
            />
            {errors.password && <p className={styles.error}>Password is incorrect</p>}

            <Button type="submit" label="Create" />
        </form>
    );
};

export default EditLectorForm;
