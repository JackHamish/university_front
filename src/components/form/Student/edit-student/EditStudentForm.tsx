import { ChangeEvent, useEffect, useState } from "react";
import Dropzone, { useDropzone } from "react-dropzone";
import styles from "./EditStudentForm.module.scss";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import Input from "../../../common/input/Input";
import Button from "../../../common/button/Button";

export type EditStudentFormFields = {
    imagePath: string;
    name: string;
    surname: string;
    email: string;
    age: number;
};

type EditStudentFormProps = {
    onSubmit: SubmitHandler<EditStudentFormFields>;
    defaultValues?: Record<string, string | number>;
};
const EditStudentForm = ({ onSubmit, defaultValues }: EditStudentFormProps) => {
    const convertToBase64 = (file: File) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    const {
        register,
        handleSubmit,
        formState: { errors },
        control,
        setValue,
    } = useForm<EditStudentFormFields>({ defaultValues });

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Controller
                control={control}
                name="imagePath"
                rules={{ required: false }}
                render={({ field: { onChange, onBlur }, fieldState }) => (
                    <Dropzone
                        noClick
                        maxFiles={1}
                        maxSize={2000000}
                        onDrop={async (acceptedFiles) => {
                            const base64 = await convertToBase64(acceptedFiles[0]);

                            setValue("imagePath", base64 as string, {
                                shouldValidate: true,
                            });
                        }}
                    >
                        {({ getRootProps, getInputProps, open, isDragActive, acceptedFiles }) => (
                            <div className={styles.dropzone}>
                                <div className={styles.dropzone__wrapper} {...getRootProps()}>
                                    {(defaultValues &&
                                        (defaultValues.imagePath as string)?.includes("base64")) ||
                                    acceptedFiles[0] ? (
                                        <img
                                            className={styles.dropzone__image}
                                            src={
                                                acceptedFiles[0]
                                                    ? URL.createObjectURL(acceptedFiles[0])
                                                    : defaultValues &&
                                                      (defaultValues.imagePath as string)
                                            }
                                            alt=""
                                        />
                                    ) : (
                                        <div className={styles.dropzone__icon}>
                                            <svg
                                                width="20"
                                                height="20"
                                                viewBox="0 0 20 20"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M8.6665 8.66663V0.666626H11.3332V8.66663H19.3332V11.3333H11.3332V19.3333H8.6665V11.3333H0.666504V8.66663H8.6665Z"
                                                    fill="white"
                                                />
                                            </svg>
                                        </div>
                                    )}

                                    <input
                                        className={styles.dropzone__input}
                                        {...getInputProps({
                                            id: "spreadsheet",
                                            onChange,
                                            onBlur,
                                        })}
                                    />
                                </div>
                                <div className={styles.dropzone__control}>
                                    <button
                                        className={styles.dropzone__btn}
                                        type="button"
                                        onClick={open}
                                    >
                                        Replace
                                    </button>
                                    {!acceptedFiles.length && <p>No file chosen</p>}
                                    <span>
                                        Must be a .jpg or .png file smaller than 10MB and at least
                                        400px by 400px.
                                    </span>
                                </div>
                            </div>
                        )}
                    </Dropzone>
                )}
            />
            <Input
                type="text"
                label="Name"
                placeholder=""
                name="name"
                register={register("name", {
                    // value: (defaultValues && (defaultValues.name as string)) || "",
                })}
            />
            {/* {errors.email && <p className={styles.error}>Email is incorrect</p>} */}

            <Input
                type="text"
                label="Surname"
                placeholder=""
                name="surname"
                register={register("surname")}
            />
            {/* {errors.email && <p className={styles.error}>Email is incorrect</p>} */}

            <Input
                type="email"
                label="Email"
                placeholder="name@mail.com"
                name="email"
                register={register("email", {
                    required: true,
                    pattern:
                        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
            />
            {errors.email && <p className={styles.error}>Email is incorrect</p>}

            <Input type="number" label="Age" placeholder="" name="age" register={register("age")} />
            {/* {errors.email && <p className={styles.error}>Email is incorrect</p>} */}

            <Button type="submit" label="Save changes" />
        </form>
    );
};

export default EditStudentForm;
