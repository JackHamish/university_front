import { ChangeEvent, useState } from "react";
import Button from "../../../common/button/Button";
import Input from "../../../common/input/Input";
import styles from "./CreateStudentForm.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

export type CreateStudentFormFields = {
    name: string;
    surname: string;
    email: string;
    age: number;
};

type CreateStudentFormProps = {
    onSubmit: SubmitHandler<CreateStudentFormFields>;
};

const CreateStudentForm = ({ onSubmit }: CreateStudentFormProps) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<CreateStudentFormFields>();

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="text"
                label="Name"
                name="name"
                register={register("name", {
                    required: true,
                })}
            />
            {errors.name && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="text"
                label="Surname"
                name="surname"
                register={register("surname", {
                    required: true,
                })}
            />
            {errors.surname && <p className={styles.error}>Name is incorrect</p>}

            <Input
                type="email"
                label="Email"
                placeholder="name@mail.com"
                name="email"
                register={register("email", {
                    required: true,
                    pattern:
                        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
            />
            {errors.email && <p className={styles.error}>Email is incorrect</p>}

            <Input
                type="number"
                label="age"
                placeholder=""
                name="age"
                register={register("age", {
                    required: true,
                })}
            />
            {errors.email && <p className={styles.error}>Email is incorrect</p>}

            <Button type="submit" label="Create" />
        </form>
    );
};

export default CreateStudentForm;
