import { ChangeEvent, useState } from "react";
import Button from "../../common/button/Button";
import CheckBox from "../../common/check-box/CheckBox";
import Input from "../../common/input/Input";
import styles from "./LoginForm.module.scss";
import { SubmitHandler, useForm } from "react-hook-form";

type SignInFormFields = {
    email: string;
    password: string;
};

type LoginFormProps = {
    onSubmit: SubmitHandler<SignInFormFields>;
};

const LoginForm = ({ onSubmit }: LoginFormProps) => {
    const [showPassword, setShowPassword] = useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<SignInFormFields>();

    const handleShowPassword = (event: ChangeEvent<HTMLInputElement>) => {
        setShowPassword(event.target.checked);
    };

    return (
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="email"
                label="Email"
                placeholder="name@mail.com"
                name="email"
                register={register("email", {
                    required: true,
                    pattern:
                        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                })}
            />
            {errors.email && <p className={styles.error}>Email is incorrect</p>}

            <Input
                type={showPassword ? "text" : "password"}
                label="Password"
                placeholder=""
                name="password"
                register={register("password", {
                    required: true,
                    pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{6,15}$/,
                })}
            />
            {errors.password && <p className={styles.error}>Password is incorrect</p>}

            <CheckBox label="Show Password" checked={showPassword} onChange={handleShowPassword} />

            <Button type="submit" label="Login" />
        </form>
    );
};

export default LoginForm;
