import { Link, Outlet, useNavigate } from "react-router-dom";
import { unwrapResult } from "@reduxjs/toolkit";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import { useEffect, useState } from "react";
import { getUserReq } from "../../../redux/auth/action.creators";

const AuthProvider = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const { loading, user, isLogged } = useAppSelector((state) => state.auth);
    const [error, setError] = useState(false);

    useEffect(() => {
        dispatch(getUserReq())
            .then(unwrapResult)
            .then(() => {})
            .catch(() => setError(true));
    }, [navigate]);

    useEffect(() => {
        if (error && !Object.keys(user).length) {
            navigate("/");
        }
    }, [error]);

    return (
        <>
            <Outlet />
        </>
    );
};

export default AuthProvider;
