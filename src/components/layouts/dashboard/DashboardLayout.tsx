import { Link, Outlet, useLocation } from "react-router-dom";
import SideBar from "../../sidebar/SideBar";
import Header from "../../header/Header";

import styles from "./DashboardLayout.module.scss";
import { useEffect } from "react";

const DashboardLayout = () => {
    const location = useLocation();

    const getTitle = () => {
        const title = location.pathname.split("/")[1];

        return title.charAt(0).toUpperCase() + title.slice(1);
    };

    return (
        <div className={styles.wrapper}>
            <SideBar />
            <main className={styles.main}>
                <Header title={getTitle()} />
                <div className={styles.content}>
                    <Outlet />
                </div>
            </main>
        </div>
    );
};

export default DashboardLayout;
