import { Link, useNavigate } from "react-router-dom";

import styles from "./SignUpPage.module.scss";
import RegisterForm from "../../form/RegisterForm/RegisterForm";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import { SubmitHandler } from "react-hook-form";
import { registerUserReq } from "../../../redux/auth/action.creators";
import { useEffect } from "react";
import { setSuccess } from "../../../redux/auth/auth.slice";
import { unwrapResult } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

type SignUpFormFields = {
    email: string;
    password: string;
    confirmPassword: string;
};

const SignUpPage = () => {
    const navigate = useNavigate();

    const dispatch = useAppDispatch();

    const { loading, error, success } = useAppSelector((state) => state.auth);

    const onSubmit: SubmitHandler<SignUpFormFields> = (data) => {
        if (data.password !== data.confirmPassword) {
            return;
        }

        data.email = data.email.toLowerCase();
        dispatch(registerUserReq(data))
            .then(unwrapResult)
            .then()
            .catch((e) => toast(e.response.data.message));
    };

    useEffect(() => {
        if (success) {
            navigate("/");
            dispatch(setSuccess());
        }
    }, [navigate, success]);

    return (
        <>
            <h1 className={styles.title}>Register your account</h1>
            <RegisterForm onSubmit={onSubmit} />
            <div className={styles.links}>
                <Link to="/">Already have account ?</Link>
            </div>
        </>
    );
};

export default SignUpPage;
