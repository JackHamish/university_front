import { Link, useNavigate, useParams, useSearchParams } from "react-router-dom";
import styles from "./ResetPassword.module.scss";
import { useEffect, useState } from "react";
import ResetPasswordForm from "../../form/ResetPasswordForm/ResetPasswordForm";
import { SubmitHandler } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import { setSuccess } from "../../../redux/auth/auth.slice";
import Button from "../../common/button/Button";
import { resetPasswordReq } from "../../../redux/auth/action.creators";
import { toast } from "react-toastify";
import { unwrapResult } from "@reduxjs/toolkit";

type ResetPasswordFormFields = {
    password: string;
    confirmPassword: string;
};

const ResetPasswordPage = () => {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();

    const dispatch = useAppDispatch();

    const { loading, error, success } = useAppSelector((state) => state.auth);

    const [passwordChanged, setPasswordChanged] = useState(false);

    useEffect(() => {
        if (success) {
            setPasswordChanged(true);
        }
    }, [navigate, success]);

    const onSubmit: SubmitHandler<ResetPasswordFormFields> = (data) => {
        const params = Object.fromEntries(searchParams.entries());
        if (params.token) {
            dispatch(resetPasswordReq({ token: params.token, password: data.password }))
                .then(unwrapResult)
                .then()
                .catch((e) => toast(e.response.data.message));
        } else {
            toast("Invalid token");
        }
    };

    const handleClickLogIn = () => {
        navigate("/");
        dispatch(setSuccess());
    };

    return (
        <>
            <h1 className={styles.title}>
                {passwordChanged ? "Password Changed" : " Reset your password"}
            </h1>
            <>
                {passwordChanged ? (
                    <>
                        <p className={styles.description}>
                            You can use your new password to log into your account
                        </p>
                        <Button onClick={handleClickLogIn} label="Log in" />
                    </>
                ) : (
                    <ResetPasswordForm onSubmit={onSubmit} />
                )}
            </>
        </>
    );
};

export default ResetPasswordPage;
