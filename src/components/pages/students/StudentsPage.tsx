import { useEffect, useState } from "react";
import DashboardHeader, {
    SearchFormFields,
} from "../../dashboard/components/DashboardHeader/DashboardHeader";
import styles from "./StudentsPage.module.scss";
import { SubmitHandler } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import TableContainer from "../../dashboard/components/TableContainer/TableContainer";
import TableRow from "../../dashboard/components/TableRow/TableRow";
import Modal from "../../modal/Modal";
import { pickBy } from "lodash";
import ReactPaginate from "react-paginate";
import { useNavigate, useSearchParams } from "react-router-dom";
import { createStudentReq, getStudentsReq } from "../../../redux/students/actions.creators";
import CreateStudentForm, {
    CreateStudentFormFields,
} from "../../form/Student/create-student/CreateStudentForm";

export const gridTemplateColumns = "0.7fr 1.2fr 1.2fr 1.5fr 1fr 1.2fr 1fr 1.5fr";

export const headerData: { name: string; value: string }[] = [
    {
        name: "Name",
        value: "name",
    },
    {
        name: "Surname",
        value: "surname",
    },
    {
        name: "Email",
        value: "email",
    },
    {
        name: "Age",
        value: "age",
    },

    {
        name: "Course",
        value: "courses",
    },

    {
        name: "Group",
        value: "group",
    },
];

const StudentsPage = () => {
    const { students, itemsCount } = useAppSelector((state) => state.students);
    const navigate = useNavigate();

    const itemsPerPage = 5;

    const [searchParams, setSearchParams] = useSearchParams();
    const [itemOffset, setItemOffset] = useState(Number(searchParams.get("offset")) || 0);
    const [pageCount, setPageCount] = useState(0);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const dispatch = useAppDispatch();

    useEffect(() => {
        searchParams.set("limit", `${itemsPerPage}`);
        searchParams.set("offset", `${itemOffset}`);

        setSearchParams(searchParams);
        dispatch(getStudentsReq(Object.fromEntries(searchParams.entries())));
    }, []);

    useEffect(() => {
        setPageCount(Math.ceil(itemsCount / itemsPerPage));
    }, [itemsCount]);

    const handleShowModal = () => {
        setIsModalOpen((prev) => !prev);
    };

    const handleClickEdit = (data: Record<string, string | number>) => {
        navigate(`${data.id}`);
    };

    const handlePageClick = ({ selected }: { selected: number }) => {
        const newOffset = selected * itemsPerPage;
        setItemOffset(newOffset);

        searchParams.set("limit", `${itemsPerPage}`);
        searchParams.set("offset", `${newOffset}`);

        setSearchParams(searchParams);

        dispatch(getStudentsReq(Object.fromEntries(searchParams.entries())));
    };

    const onSearchSubmit: SubmitHandler<SearchFormFields> = (data) => {
        const params = {
            sortField: data?.sort?.split("_")[0] === "all" ? "" : data?.sort?.split("_")[0],
            sortOrder: data?.sort?.split("_")[1] || "",
            name: data.name,
        };

        const filteredParams = pickBy(params, (value) => value && value.length > 0);

        if (filteredParams?.name) {
            searchParams.set("name", `${params.name}`);
        } else {
            searchParams.delete("name");
        }

        if (filteredParams?.sortField) {
            searchParams.set("sortField", `${params.sortField}`);
        } else {
            searchParams.delete("sortField");
        }

        if (filteredParams?.sortOrder) {
            searchParams.set("sortOrder", `${params.sortOrder}`);
        } else {
            searchParams.delete("sortOrder");
        }

        setSearchParams(searchParams);

        dispatch(getStudentsReq(Object.fromEntries(searchParams.entries())));
    };

    const onCreateSubmit: SubmitHandler<CreateStudentFormFields> = (data) => {
        data.age = +data.age;
        dispatch(createStudentReq(data)).then(() =>
            dispatch(getStudentsReq(Object.fromEntries(searchParams.entries())))
        );
        setIsModalOpen(false);
    };

    return (
        <>
            <div className={styles.wrapper}>
                <DashboardHeader
                    buttonLabel="Add new student"
                    onClick={handleShowModal}
                    onSubmit={onSearchSubmit}
                    defaultValues={{
                        name: Object.fromEntries(searchParams.entries())?.name,
                        sort: Object.fromEntries(searchParams.entries())?.sortField
                            ? `${Object.fromEntries(searchParams.entries())?.sortField}_${
                                  Object.fromEntries(searchParams.entries()).sortOrder
                              }`
                            : "all",
                    }}
                />
                <TableContainer
                    startPadding={true}
                    endPadding={true}
                    gridTemplateColumns={gridTemplateColumns}
                    header={headerData}
                >
                    {students.map((row, i) => {
                        const filteredRow = pickBy(row, (_, key) =>
                            headerData.some(
                                (header) => header.value.toLowerCase() === key.toLowerCase()
                            )
                        );

                        return (
                            <TableRow
                                key={i}
                                gridTemplateColumns={gridTemplateColumns}
                                filteredData={filteredRow}
                                imagePath={row.imagePath || "null"}
                                onClick={(data) => handleClickEdit(data)}
                                fullData={row}
                            />
                        );
                    })}
                </TableContainer>
            </div>

            {pageCount > 0 && (
                <ReactPaginate
                    previousLabel={"← Previous"}
                    nextLabel={"Next →"}
                    pageCount={pageCount}
                    initialPage={Math.floor(itemOffset / itemsPerPage)}
                    onPageChange={handlePageClick}
                    disableInitialCallback={true}
                    containerClassName={styles.pagination}
                    previousLinkClassName={styles.pagination__link}
                    nextLinkClassName={styles.pagination__link}
                    disabledClassName={styles.pagination__link__disabled}
                    activeClassName={styles.pagination__link__active}
                />
            )}

            {isModalOpen && (
                <Modal title={"Add new Student"} onClick={handleShowModal}>
                    <CreateStudentForm onSubmit={onCreateSubmit} />
                </Modal>
            )}
        </>
    );
};

export default StudentsPage;
