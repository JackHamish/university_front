import { useEffect, useState } from "react";
import DashboardHeader, {
    SearchFormFields,
} from "../../dashboard/components/DashboardHeader/DashboardHeader";
import styles from "./LectorsPage.module.scss";
import { SubmitHandler } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import {
    createLectorReq,
    getLectorsReq,
    updateLectorReq,
} from "../../../redux/lectors/actions.creators";
import TableContainer from "../../dashboard/components/TableContainer/TableContainer";
import TableRow from "../../dashboard/components/TableRow/TableRow";
import CreateLectorForm, {
    CreateLectorFormFields,
} from "../../form/Lector/CreateLectorForm/CreateLectorForm";
import Modal from "../../modal/Modal";
import EditLectorForm, {
    EditLectorFormFields,
} from "../../form/Lector/EditLectorForm/EditLectorForm";
import { pickBy } from "lodash";
import ReactPaginate from "react-paginate";
import { useSearchParams } from "react-router-dom";
import { useQuery } from "../../../utils/hooks/useQuery";

export const gridTemplateColumns = "0.7fr 0.7fr 1fr 1fr 1.5fr";

export const headerData: { name: string }[] = [
    {
        name: "Name",
    },
    {
        name: "Surname",
    },
    {
        name: "Email",
    },
    {
        name: "Password",
    },
];

const LectorsPage = () => {
    const { lectors, itemsCount } = useAppSelector((state) => state.lectors);

    const itemsPerPage = 5;
    const [searchParams, setSearchParams] = useSearchParams();
    const [itemOffset, setItemOffset] = useState(Number(searchParams.get("offset")) || 0);
    const [pageCount, setPageCount] = useState(0);

    const [isModalOpen, setIsModalOpen] = useState(false);

    const [isCreateModal, setIsCreateModal] = useState(true);
    const [editedLector, setEditedLector] = useState<{ id?: string }>({});

    const dispatch = useAppDispatch();

    useEffect(() => {
        searchParams.set("limit", `${itemsPerPage}`);
        searchParams.set("offset", `${itemOffset}`);

        setSearchParams(searchParams);
        dispatch(getLectorsReq(Object.fromEntries(searchParams.entries())));
    }, []);

    useEffect(() => {
        setPageCount(Math.ceil(itemsCount / itemsPerPage));
    }, [itemsCount]);

    const handleShowModal = () => {
        setIsModalOpen((prev) => !prev);
        setIsCreateModal(true);
    };

    const handleClickEdit = (data: Record<string, string | number>) => {
        setIsModalOpen((prev) => !prev);
        setIsCreateModal(false);
        setEditedLector(data);
    };

    const handlePageClick = ({ selected }: { selected: number }) => {
        const newOffset = selected * itemsPerPage;
        setItemOffset(newOffset);

        searchParams.set("limit", `${itemsPerPage}`);
        searchParams.set("offset", `${newOffset}`);

        setSearchParams(searchParams);

        dispatch(getLectorsReq(Object.fromEntries(searchParams.entries())));
    };

    const onSearchSubmit: SubmitHandler<SearchFormFields> = (data) => {
        const params = {
            sortField: data?.sort?.split("_")[0] === "all" ? "" : data?.sort?.split("_")[0],
            sortOrder: data?.sort?.split("_")[1] || "",
            name: data.name,
        };

        const filteredParams = pickBy(params, (value) => value && value.length > 0);

        if (filteredParams?.name) {
            searchParams.set("name", `${params.name}`);
        } else {
            searchParams.delete("name");
        }

        if (filteredParams?.sortField) {
            searchParams.set("sortField", `${params.sortField}`);
        } else {
            searchParams.delete("sortField");
        }

        if (filteredParams?.sortOrder) {
            searchParams.set("sortOrder", `${params.sortOrder}`);
        } else {
            searchParams.delete("sortOrder");
        }

        setSearchParams(searchParams);

        dispatch(getLectorsReq(Object.fromEntries(searchParams.entries())));
    };

    const onCreateSubmit: SubmitHandler<CreateLectorFormFields> = (data) => {
        dispatch(createLectorReq(data)).then(() =>
            dispatch(getLectorsReq(Object.fromEntries(searchParams.entries())))
        );
        setIsModalOpen(false);
    };

    const onEditSubmit: SubmitHandler<EditLectorFormFields> = (data) => {
        const filteredData = pickBy(data, (value) => value.length > 0);

        const id = editedLector.id!;

        dispatch(updateLectorReq({ id, data: filteredData })).then(() =>
            dispatch(getLectorsReq(Object.fromEntries(searchParams.entries())))
        );
        setIsModalOpen(false);
    };

    return (
        <>
            <div className={styles.wrapper}>
                <DashboardHeader
                    buttonLabel="Add new lector"
                    onClick={handleShowModal}
                    onSubmit={onSearchSubmit}
                    defaultValues={{
                        name: Object.fromEntries(searchParams.entries())?.name,
                        sort: Object.fromEntries(searchParams.entries())?.sortField
                            ? `${Object.fromEntries(searchParams.entries())?.sortField}_${
                                  Object.fromEntries(searchParams.entries()).sortOrder
                              }`
                            : "all",
                    }}
                />
                <TableContainer
                    endPadding={true}
                    gridTemplateColumns={gridTemplateColumns}
                    header={headerData}
                >
                    {lectors.map((row, i) => {
                        const filteredRow = pickBy(row, (_, key) =>
                            headerData.some(
                                (header) => header.name.toLowerCase() === key.toLowerCase()
                            )
                        );

                        return (
                            <TableRow
                                key={i}
                                gridTemplateColumns={gridTemplateColumns}
                                filteredData={filteredRow}
                                onClick={(data) => handleClickEdit(data)}
                                fullData={row}
                            />
                        );
                    })}
                </TableContainer>
            </div>

            {pageCount > 0 && (
                <ReactPaginate
                    previousLabel={"← Previous"}
                    nextLabel={"Next →"}
                    pageCount={pageCount}
                    initialPage={Math.floor(itemOffset / itemsPerPage)}
                    onPageChange={handlePageClick}
                    disableInitialCallback={true}
                    containerClassName={styles.pagination}
                    previousLinkClassName={styles.pagination__link}
                    nextLinkClassName={styles.pagination__link}
                    disabledClassName={styles.pagination__link__disabled}
                    activeClassName={styles.pagination__link__active}
                />
            )}

            {isModalOpen && (
                <Modal
                    title={isCreateModal ? "Add new lector" : "Update lector"}
                    onClick={handleShowModal}
                >
                    <>
                        {isCreateModal ? (
                            <CreateLectorForm onSubmit={onCreateSubmit} />
                        ) : (
                            <EditLectorForm onSubmit={onEditSubmit} defaultValues={editedLector} />
                        )}
                    </>
                </Modal>
            )}
        </>
    );
};

export default LectorsPage;
