import { Link, useNavigate } from "react-router-dom";

import styles from "./ForgotPassword.module.scss";

import ForgotPasswordForm from "../../form/ForgotPasswordForm/ForgotPasswordForm";
import { SubmitHandler } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import { useEffect } from "react";
import { setSuccess } from "../../../redux/auth/auth.slice";
import { forgotPasswordReq } from "../../../redux/auth/action.creators";
import Button from "../../common/button/Button";
import { toast } from "react-toastify";
import { unwrapResult } from "@reduxjs/toolkit";

type ForgotPasswordFormFields = {
    email: string;
};

const ForgotPasswordPage = () => {
    const navigate = useNavigate();

    const dispatch = useAppDispatch();

    const { loading, error, success } = useAppSelector((state) => state.auth);

    useEffect(() => {
        // if (success) {
        //     navigate("/");
        //     dispatch(setSuccess());
        // }
    }, [navigate, success]);

    const onSubmit: SubmitHandler<ForgotPasswordFormFields> = (data) => {
        dispatch(forgotPasswordReq(data))
            .then(unwrapResult)
            .then()
            .catch((e) => toast(e.response.data.message));
    };

    const handleClickBack = () => {
        navigate("/");
        dispatch(setSuccess());
    };

    return (
        <>
            {success ? (
                <>
                    <h1 className={styles.title}>Reset password</h1>
                    <p className={styles.description}>
                        A password reset email has been sent to your email address.
                    </p>
                    <Button label="Back" onClick={handleClickBack} />
                </>
            ) : (
                <>
                    <h1 className={styles.title}>Reset password</h1>
                    <p className={styles.description}>
                        Don't worry, happens to the best of us. Enter the email address associated
                        with your account and we'll send you a link to reset.
                    </p>
                    <ForgotPasswordForm onSubmit={onSubmit} />
                    <Link className={styles.cancel} to="/">
                        Cancel
                    </Link>
                </>
            )}{" "}
            <></>
        </>
    );
};

export default ForgotPasswordPage;
