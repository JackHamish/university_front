import { Link, useNavigate } from "react-router-dom";

import styles from "./SignInPage.module.scss";
import LoginForm from "../../form/LoginForm/LoginForm";
import { SubmitHandler, useForm } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import { loginUserReq } from "../../../redux/auth/action.creators";
import { useEffect } from "react";
import { unwrapResult } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

type SignInFormFields = {
    email: string;
    password: string;
};

const SignInPage = () => {
    const dispatch = useAppDispatch();

    const navigate = useNavigate();

    const { loading, error, isLogged } = useAppSelector((state) => state.auth);

    const onSubmit: SubmitHandler<SignInFormFields> = (data) => {
        dispatch(loginUserReq(data))
            .then(unwrapResult)
            .then()
            .catch((e) => toast(e.response.data.message));
    };

    useEffect(() => {
        if (isLogged) {
            navigate("/lectors");
        }
    }, [navigate, isLogged]);

    return (
        <>
            <h1 className={styles.title}>Welcome!</h1>
            <LoginForm onSubmit={onSubmit} />
            <div className={styles.links}>
                <Link to="/sign-up">Don't have account ?</Link>
                <Link to="/forgot-password">Forgot password ?</Link>
            </div>
        </>
    );
};

export default SignInPage;
