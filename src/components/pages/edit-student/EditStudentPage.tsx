import { Controller, SubmitHandler, useForm } from "react-hook-form";
import DashboardHeader from "../../dashboard/components/DashboardHeader/DashboardHeader";
import EditStudentForm, {
    EditStudentFormFields,
} from "../../form/Student/edit-student/EditStudentForm";
import Header from "../../header/Header";
import styles from "./EditStudentPage.module.scss";
import { useNavigate, useParams } from "react-router";
import Select, { MultiValue } from "react-select";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../utils/hooks";
import { getStudentByIdReq, updateStudentReq } from "../../../redux/students/actions.creators";
import { getGroupsReq } from "../../../redux/groups/action.creators";
import { unwrapResult } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import { clearStudent } from "../../../redux/students/students.slice";

const sort = [
    { value: "name_ASC", label: "A-Z" },
    { value: "name_DESC", label: "Z-A" },
    { value: "createdAt_ASC", label: "Newest First" },
    { value: "createdAt_DESC", label: "Latest First" },
    { value: "all", label: "All" },
];

const EditStudentPage = () => {
    const { student } = useAppSelector((state) => state.students);
    const { groups } = useAppSelector((state) => state.groups);
    const dispatch = useAppDispatch();
    const params = useParams();

    const navigate = useNavigate();
    const [groupOptions, setGroupOptions] = useState<{ label: string; value: string }[]>();
    const [selectedCourses, setSelectedCourses] = useState();
    const [selectedGroup, setSelectedGroup] = useState<{ label: string; value: string }>();

    const onSubmit: SubmitHandler<EditStudentFormFields> = (data) => {
        if (params.id) {
            data.age = +data.age;
            dispatch(updateStudentReq({ id: params.id, data }))
                .then(unwrapResult)
                .then(() => dispatch(getStudentByIdReq(params.id!)))
                .catch((e) => toast(e.response.data.message));
        }
    };

    useEffect(() => {
        if (params.id) {
            dispatch(getStudentByIdReq(params.id));
            dispatch(getGroupsReq({}));

            const options = groups.map((group) => ({
                label: group.name,
                value: group.id,
            }));

            setGroupOptions(options);

            setSelectedGroup(options?.find((option) => option.value === student.groupId));
        }
    }, []);

    const handleCourseChange = (selected: any) => {
        setSelectedCourses(selected);
    };

    const handleGroupChange = (selected: any) => {
        setSelectedGroup(selected);
        if (params.id) {
            const data = { groupId: +selected?.value! };

            dispatch(updateStudentReq({ id: params.id, data }))
                .then(unwrapResult)
                .then(() => dispatch(getStudentByIdReq(params.id!)))
                .catch((e) => toast(e.response.data.message));
        }
    };

    const handleClickBack = () => {
        navigate("/students");
        dispatch(clearStudent());
    };

    return (
        <>
            <Header
                image={
                    <svg
                        width="42"
                        height="43"
                        viewBox="0 0 42 43"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M21 24.8117L10.5118 30.8084L0 24.8117L10.5118 18.815L21 24.8117Z"
                            fill="#D5B5FF"
                        />
                        <path
                            d="M0 24.8116V36.805L10.5118 42.8017V30.8084L0 24.8116Z"
                            fill="#A965FF"
                        />
                        <path
                            d="M20.9999 24.8116V36.805L10.5117 42.8017V30.8084L20.9999 24.8116Z"
                            fill="#7101FF"
                        />
                        <path
                            d="M42 24.8117L31.4882 30.8084L21 24.8117L31.4882 18.815L42 24.8117Z"
                            fill="#D5B5FF"
                        />
                        <path
                            d="M21 24.8116V36.805L31.4882 42.8017V30.8084L21 24.8116Z"
                            fill="#A965FF"
                        />
                        <path
                            d="M42.0001 24.8116V36.805L31.4883 42.8017V30.8084L42.0001 24.8116Z"
                            fill="#7101FF"
                        />
                        <path
                            d="M31.4882 6.79832L20.9999 12.795L10.5117 6.79832L20.9999 0.801666L31.4882 6.79832Z"
                            fill="#D5B5FF"
                        />
                        <path
                            d="M10.5117 6.79828V18.8149L20.9999 24.8116V12.7949L10.5117 6.79828Z"
                            fill="#A965FF"
                        />
                        <path
                            d="M31.4882 6.79828V18.8149L21 24.8116V12.7949L31.4882 6.79828Z"
                            fill="#7101FF"
                        />
                    </svg>
                }
            />
            {Object.keys(student)?.length ? (
                <main className={styles.wrapper}>
                    <div className={styles.nav}>
                        <button onClick={handleClickBack} className={styles.button}>
                            <svg
                                className={styles.button__icon}
                                width="11"
                                height="18"
                                viewBox="0 0 11 18"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M9.0348 0.515015L0.549805 9.00001L9.0348 17.485L10.4498 16.071L3.3778 9.00001L10.4498 1.92901L9.0348 0.515015Z"
                                    fill="#505E68"
                                />
                            </svg>

                            <span className={styles.button__label}>Back</span>
                        </button>
                    </div>
                    <div className={styles.info}>
                        <h2 className={styles.title}> Personal information </h2>
                        <EditStudentForm
                            defaultValues={{
                                name: student.name,
                                surname: student.surname,
                                email: student.email,
                                age: student.age,
                                imagePath: student.imagePath,
                            }}
                            onSubmit={onSubmit}
                        />
                    </div>
                    <div className={styles.info}>
                        <div className={styles.info__items}>
                            <h2 className={styles.title}> Courses and groups </h2>
                            <span className={styles.sort__label}>Course</span>
                            <Select
                                isDisabled
                                className={styles.sort}
                                id="courses"
                                options={groups.map((group) => ({
                                    label: group.name,
                                    value: group.id,
                                }))}
                                closeMenuOnSelect={false}
                                value={selectedCourses}
                                onChange={(selected) => handleCourseChange(selected)}
                                isMulti
                                isSearchable={false}
                                styles={{
                                    indicatorSeparator: (base, state) => ({
                                        ...base,
                                        display: "none",
                                    }),
                                    option: (base, state) => ({
                                        ...base,
                                        color:
                                            state.isFocused || state.isSelected
                                                ? "#3b4360"
                                                : "#9f9fb6",
                                    }),
                                    control: (base, state) => ({
                                        ...base,
                                        transition: "none",

                                        ":hover": {
                                            border: "1px solid #505E68",
                                            borderBottom: state.selectProps.menuIsOpen
                                                ? "none"
                                                : "1px solid #505E68",
                                        },
                                        border: "1px solid #505E68",

                                        borderBottom: state.selectProps.menuIsOpen
                                            ? "none"
                                            : "1px solid #505E68",
                                    }),
                                    menu: (base, state) => ({
                                        ...base,

                                        border: "1px solid #505E68",
                                        borderRadius: state.selectProps.menuIsOpen
                                            ? " 0px 0px 0px 0px"
                                            : "0px",
                                        borderTop: state.selectProps.menuIsOpen
                                            ? "none"
                                            : "1px solid #505E68",
                                    }),

                                    dropdownIndicator: (base, state) => ({
                                        ...base,
                                        transition: "all .2s ease",
                                        transform: state.selectProps.menuIsOpen
                                            ? "rotate(180deg)"
                                            : "none",
                                    }),
                                }}
                            />

                            <span className={styles.sort__label}>Group</span>
                            <Select
                                className={styles.sort}
                                id="group"
                                options={groupOptions}
                                value={groupOptions?.find(
                                    (option) => option.value === student.groupId
                                )}
                                onChange={(val) => handleGroupChange(val)}
                                isSearchable={false}
                                styles={{
                                    indicatorSeparator: (base, state) => ({
                                        ...base,
                                        display: "none",
                                    }),
                                    option: (base, state) => ({
                                        ...base,
                                        color:
                                            state.isFocused || state.isSelected
                                                ? "#3b4360"
                                                : "#9f9fb6",
                                    }),
                                    control: (base, state) => ({
                                        ...base,
                                        transition: "none",

                                        ":hover": {
                                            border: "1px solid #505E68",
                                            borderBottom: state.selectProps.menuIsOpen
                                                ? "none"
                                                : "1px solid #505E68",
                                        },
                                        border: "1px solid #505E68",

                                        borderBottom: state.selectProps.menuIsOpen
                                            ? "none"
                                            : "1px solid #505E68",
                                    }),
                                    menu: (base, state) => ({
                                        ...base,

                                        border: "1px solid #505E68",
                                        borderRadius: state.selectProps.menuIsOpen
                                            ? " 0px 0px 0px 0px"
                                            : "0px",
                                        borderTop: state.selectProps.menuIsOpen
                                            ? "none"
                                            : "1px solid #505E68",
                                    }),

                                    dropdownIndicator: (base, state) => ({
                                        ...base,
                                        transition: "all .2s ease",
                                        transform: state.selectProps.menuIsOpen
                                            ? "rotate(180deg)"
                                            : "none",
                                    }),
                                }}
                            />
                        </div>
                    </div>
                </main>
            ) : (
                <></>
            )}
        </>
    );
};

export default EditStudentPage;
