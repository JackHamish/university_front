import styles from "./Header.module.scss";
import userIcon from "../../assets/user.png";
export interface IHeaderProps {
    title?: string;
    image?: any;
}

const Header = ({ title, image }: IHeaderProps) => {
    return (
        <header className={styles.header}>
            <div className={styles.logo__wrapper}>
                {title && <h1 className={styles.header__title}> {title}</h1>}
                {image && <>{image}</>}
            </div>
            <div className={styles.user}>
                <img className={styles.user__image} src={userIcon} alt="user icon" />
            </div>
        </header>
    );
};

export default Header;
