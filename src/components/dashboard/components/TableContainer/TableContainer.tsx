import { ReactNode } from "react";
import styles from "./TableContainer.module.scss";

import clsx from "classnames";

type TableContainerProps = {
    gridTemplateColumns: string;
    header: { name: string }[];
    children: ReactNode;
    startPadding?: boolean;
    endPadding?: boolean;
};

const TableContainer = ({
    gridTemplateColumns,
    header,
    children,
    startPadding,
    endPadding,
}: TableContainerProps) => {
    return (
        <>
            <div className={styles.header} style={{ gridTemplateColumns }}>
                {startPadding && (
                    <div
                        className={clsx(styles.header__item, {
                            [styles.header__item__empty]: true,
                        })}
                    >
                        empty
                    </div>
                )}
                {header.map((item, i) => (
                    <div key={i} className={styles.header__item}>
                        {item.name}
                    </div>
                ))}
                {endPadding && (
                    <div
                        className={clsx(styles.header__item, {
                            [styles.header__item__empty]: true,
                        })}
                    >
                        empty
                    </div>
                )}
            </div>
            {children}
        </>
    );
};

export default TableContainer;
