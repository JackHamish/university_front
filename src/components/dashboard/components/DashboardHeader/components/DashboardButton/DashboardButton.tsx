import { useState } from "react";
import styles from "./DashboardButton.module.scss";
interface DashboardButtonProps {
    label: string;
    onClick?: () => void;
}

const DashboardButton = ({ label, onClick }: DashboardButtonProps) => {
    return (
        <button onClick={onClick} className={styles.button}>
            <svg
                className={styles.button__icon}
                width="13"
                height="13"
                viewBox="0 0 15 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M5.70825 5.70834V0.958344H7.29158V5.70834H12.0416V7.29168H7.29158V12.0417H5.70825V7.29168H0.958252V5.70834H5.70825Z"
                    fill="white"
                />
            </svg>

            <span className={styles.button__label}>{label}</span>
        </button>
    );
};

export default DashboardButton;
