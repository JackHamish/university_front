import { Controller, SubmitHandler, useForm, useWatch } from "react-hook-form";
import styles from "./DashboardHeader.module.scss";
import DashboardButton from "./components/DashboardButton/DashboardButton";
import DashboardInput from "./components/DashboardInput/DashboardInput";
import debounce from "lodash/debounce";
import { ChangeEvent, useEffect } from "react";
import Select from "react-select";
import clsx from "classnames";
export type SearchFormFields = {
    sort?: string | undefined;
    name?: string | undefined;
};
interface DashboardHeadersProps {
    buttonLabel: string;
    onClick?: () => void;
    onSubmit: SubmitHandler<SearchFormFields>;
    defaultValues?: {
        name?: string;
        sort?: string;
    };
}

const sort = [
    { value: "name_ASC", label: "A-Z" },
    { value: "name_DESC", label: "Z-A" },
    { value: "createdAt_ASC", label: "Newest First" },
    { value: "createdAt_DESC", label: "Latest First" },
    { value: "all", label: "All" },
];

const DashboardHeader = ({
    buttonLabel,
    onClick,
    onSubmit,
    defaultValues,
}: DashboardHeadersProps) => {
    const {
        register,
        handleSubmit,
        watch,
        trigger,
        control,
        formState: { errors },
    } = useForm<SearchFormFields>({ defaultValues });

    const [sortValue, nameValue] = watch(["sort", "name"]);

    useEffect(() => {
        const subscription = watch((data) => onSubmit(data));

        return () => {
            subscription.unsubscribe();
        };
    }, [sortValue, nameValue]);

    return (
        <div className={styles.wrapper}>
            <span className={styles.sort__title}>Sort by</span>
            <form className={styles.search} onSubmit={handleSubmit(onSubmit)}>
                <Controller
                    control={control}
                    name="sort"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                        <Select
                            className={clsx(styles.sort)}
                            ref={ref}
                            id="sort"
                            options={sort}
                            value={sort.find((c) => c.value === value)}
                            onChange={(val) => onChange(val?.value)}
                            onBlur={onBlur}
                            isSearchable={false}
                            styles={{
                                indicatorSeparator: (base, state) => ({
                                    ...base,
                                    display: "none",
                                }),
                                option: (base, state) => ({
                                    ...base,
                                    color:
                                        state.isFocused || state.isSelected ? "#3b4360" : "#9f9fb6",
                                }),
                                control: (base, state) => ({
                                    ...base,
                                    transition: "none",

                                    ":hover": {
                                        border: "1px solid #e4e7ed",
                                        borderBottom: state.selectProps.menuIsOpen
                                            ? "none"
                                            : "1px solid #e4e7ed",
                                    },
                                    border: "1px solid #e4e7ed",
                                    borderRadius: state.selectProps.menuIsOpen
                                        ? "6px 6px 0px 0px"
                                        : "6px",
                                    borderBottom: state.selectProps.menuIsOpen
                                        ? "none"
                                        : "1px solid #e4e7ed",
                                }),
                                menu: (base, state) => ({
                                    ...base,

                                    border: "1px solid #e4e7ed",
                                    borderRadius: state.selectProps.menuIsOpen
                                        ? " 0px 0px 6px 6px"
                                        : "6px",
                                    borderTop: state.selectProps.menuIsOpen
                                        ? "none"
                                        : "1px solid #e4e7ed",
                                }),

                                dropdownIndicator: (base, state) => ({
                                    ...base,
                                    transition: "all .2s ease",
                                    transform: state.selectProps.menuIsOpen
                                        ? "rotate(180deg)"
                                        : "none",
                                }),
                            }}
                        />
                    )}
                />
                <Controller
                    control={control}
                    name="name"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                        <DashboardInput
                            name="search"
                            placeholder="Search"
                            onChange={debounce((e) => onChange(e), 500)}
                            onBlur={onBlur}
                        />
                    )}
                />
            </form>
            <DashboardButton label={buttonLabel} onClick={onClick} />
        </div>
    );
};

export default DashboardHeader;
