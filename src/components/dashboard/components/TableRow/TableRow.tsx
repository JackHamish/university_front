import { ReactNode, useEffect } from "react";

import styles from "./TableRow.module.scss";

type Item = {
    name: string;
};

type TableRowProps = {
    gridTemplateColumns: string;
    filteredData: Record<string, any>;
    fullData?: Record<string, any>;
    onClick?: (data: Record<string, string | number>) => void;
    imagePath?: string;
};

const TableRow = ({
    gridTemplateColumns,
    filteredData,
    onClick,
    fullData,
    imagePath,
}: TableRowProps) => {
    const renderValue = (value: any): string => {
        if (Array.isArray(value)) {
            return value.map((item) => renderValue(item)).join(", ");
        } else if (typeof value === "object" && value !== null) {
            if ("name" in value) {
                return renderValue(value.name);
            } else {
                return Object.values(value)
                    .map((item) => renderValue(item))
                    .join(", ");
            }
        }
        return value || "null";
    };

    return (
        <>
            <div className={styles.row} style={{ gridTemplateColumns }}>
                {imagePath && (
                    <div className={styles.row__item}>
                        {imagePath?.includes("base64") ? (
                            <img
                                className={styles.avatar__image}
                                src={imagePath}
                                alt="student image"
                            />
                        ) : (
                            <div className={styles.avatar}>
                                {filteredData?.name.trim().toUpperCase().split("").slice(0, 1)}
                            </div>
                        )}
                    </div>
                )}

                {Object.keys(filteredData).map((key, i) => (
                    <div key={i} className={styles.row__item}>
                        {renderValue(filteredData[key])}
                    </div>
                ))}

                <div className={styles.row__item__edit}>
                    <button onClick={() => onClick!(fullData!)} className={styles.edit__btn}>
                        <svg
                            className={styles.edit__btn__icon}
                            width="19"
                            height="19"
                            viewBox="0 0 19 19"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M13.7574 0.996658L6.29145 8.4626L6.29886 12.7097L10.537 12.7023L18 5.2393V17.9967C18 18.5489 17.5523 18.9967 17 18.9967H1C0.44772 18.9967 0 18.5489 0 17.9967V1.99666C0 1.44438 0.44772 0.996658 1 0.996658H13.7574ZM17.4853 0.097168L18.8995 1.51138L9.7071 10.7038L8.2954 10.7062L8.2929 9.2896L17.4853 0.097168Z"
                                fill="#BBB0C8"
                            />
                        </svg>
                    </button>
                </div>
            </div>
        </>
    );
};

export default TableRow;
