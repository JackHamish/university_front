import { Link, NavLink } from "react-router-dom";
import clsx from "classnames";
import styles from "./SideBarLink.module.scss";

interface SideBarLinkProps {
    path: string;
    title: string;
    icon: any;
}

const SideBarLink = ({ path, title, icon }: SideBarLinkProps) => {
    return (
        <NavLink
            className={({ isActive }) =>
                clsx(styles.item, {
                    [styles.item__active]: isActive,
                })
            }
            to={path}
        >
            <div className={styles.item__icon}>{icon}</div>
            <span className={styles.item__label}>{title}</span>
        </NavLink>
    );
};

export default SideBarLink;
