import { useLocation, useNavigate } from "react-router-dom";
import { logOut } from "../../redux/auth/auth.slice";
import { useAppDispatch } from "../../utils/hooks";
import styles from "./SideBar.module.scss";
import SideBarLink from "./components/SideBarLink/SideBarLink";
import { sideBarData } from "./sideBarData";
import clsx from "classnames";

const SideBar = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const handleLogOut = () => {
        dispatch(logOut());
        localStorage.removeItem("userToken");
        navigate("/");
    };

    return (
        <nav className={styles.sidebar}>
            <div className={styles.sidebar__container}>
                <div className={styles.sidebar__logo__container}>
                    <svg
                        // className={styles.logo}
                        width="42"
                        height="43"
                        viewBox="0 0 42 43"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M21 24.5816L10.5118 30.7211L0 24.5816L10.5118 18.4422L21 24.5816Z"
                            fill="white"
                        />
                        <path d="M0 24.5816V36.8605L10.5118 43V30.7211L0 24.5816Z" fill="#DBD2E6" />
                        <path
                            d="M21 24.5816V36.8605L10.5118 43V30.7211L21 24.5816Z"
                            fill="#B4A1C8"
                        />
                        <path
                            d="M42 24.5816L31.4882 30.7211L21 24.5816L31.4882 18.4422L42 24.5816Z"
                            fill="white"
                        />
                        <path
                            d="M21 24.5816V36.8605L31.4882 43V30.7211L21 24.5816Z"
                            fill="#DBD2E5"
                        />
                        <path
                            d="M42 24.5816V36.8605L31.4882 43V30.7211L42 24.5816Z"
                            fill="#B4A1C8"
                        />
                        <path
                            d="M31.4882 6.13943L21 12.2789L10.5118 6.13943L21 0L31.4882 6.13943Z"
                            fill="white"
                        />
                        <path
                            d="M10.5118 6.13943V18.4422L21 24.5816V12.2789L10.5118 6.13943Z"
                            fill="#DBD2E5"
                        />
                        <path
                            d="M31.4882 6.13943V18.4422L21 24.5816V12.2789L31.4882 6.13943Z"
                            fill="#B4A1C8"
                        />
                    </svg>
                </div>

                <div
                    className={clsx(styles.sidebar__item, {
                        [styles.sidebar__item__primary]: true,
                    })}
                >
                    <svg
                        width="14"
                        height="14"
                        viewBox="0 0 14 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M2 0C0.895431 0 0 0.89543 0 2V4C0 5.10457 0.89543 6 2 6H4C5.10457 6 6 5.10457 6 4V2C6 0.895431 5.10457 0 4 0H2ZM2 8C0.895431 8 0 8.89543 0 10V12C0 13.1046 0.89543 14 2 14H4C5.10457 14 6 13.1046 6 12V10C6 8.89543 5.10457 8 4 8H2ZM8 2C8 0.89543 8.89543 0 10 0H12C13.1046 0 14 0.895431 14 2V4C14 5.10457 13.1046 6 12 6H10C8.89543 6 8 5.10457 8 4V2ZM10 8C8.89543 8 8 8.89543 8 10V12C8 13.1046 8.89543 14 10 14H12C13.1046 14 14 13.1046 14 12V10C14 8.89543 13.1046 8 12 8H10Z"
                            fill="#EAE6F2"
                        />
                    </svg>

                    <span className={styles.sidebar__item__label}>Dashboard</span>
                </div>

                <div className={styles.sidebar__container}>
                    <div className={styles.sidebar__items}>
                        {sideBarData.map((link) => (
                            <SideBarLink
                                key={link.title}
                                title={link.title}
                                path={link.path}
                                icon={link.icon}
                            />
                        ))}
                    </div>

                    <button className={styles.sidebar__footer} onClick={handleLogOut}>
                        <svg
                            className={styles.sidebar__item__icon}
                            width="18"
                            height="18"
                            viewBox="0 0 18 18"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M12.6 1.5H10.65C8.25 1.5 6.75 3 6.75 5.4V8.4375H11.4375C11.745 8.4375 12 8.6925 12 9C12 9.3075 11.745 9.5625 11.4375 9.5625H6.75V12.6C6.75 15 8.25 16.5 10.65 16.5H12.5925C14.9925 16.5 16.4925 15 16.4925 12.6V5.4C16.5 3 15 1.5 12.6 1.5Z"
                                fill="white"
                            />
                            <path
                                d="M3.42002 8.43738L4.97252 6.88488C5.08502 6.77238 5.13752 6.62988 5.13752 6.48738C5.13752 6.34488 5.08502 6.19488 4.97252 6.08988C4.75502 5.87238 4.39502 5.87238 4.17752 6.08988L1.66502 8.60238C1.44752 8.81988 1.44752 9.17988 1.66502 9.39738L4.17752 11.9099C4.39502 12.1274 4.75502 12.1274 4.97252 11.9099C5.19002 11.6924 5.19002 11.3324 4.97252 11.1149L3.42002 9.56238H6.75002V8.43738H3.42002Z"
                                fill="white"
                            />
                        </svg>

                        <span className={styles.sidebar__item__label}>Log out</span>
                    </button>
                </div>
            </div>
        </nav>

        // <div className={styles.sidebar__wrapper}>
        //     <div className={styles.logo__wrapper}>
        //         <svg
        //             className={styles.logo}
        //             width="42"
        //             height="43"
        //             viewBox="0 0 42 43"
        //             fill="none"
        //             xmlns="http://www.w3.org/2000/svg"
        //         >
        //             <path
        //                 d="M21 24.5816L10.5118 30.7211L0 24.5816L10.5118 18.4422L21 24.5816Z"
        //                 fill="white"
        //             />
        //             <path d="M0 24.5816V36.8605L10.5118 43V30.7211L0 24.5816Z" fill="#DBD2E6" />
        //             <path d="M21 24.5816V36.8605L10.5118 43V30.7211L21 24.5816Z" fill="#B4A1C8" />
        //             <path
        //                 d="M42 24.5816L31.4882 30.7211L21 24.5816L31.4882 18.4422L42 24.5816Z"
        //                 fill="white"
        //             />
        //             <path d="M21 24.5816V36.8605L31.4882 43V30.7211L21 24.5816Z" fill="#DBD2E5" />
        //             <path d="M42 24.5816V36.8605L31.4882 43V30.7211L42 24.5816Z" fill="#B4A1C8" />
        //             <path
        //                 d="M31.4882 6.13943L21 12.2789L10.5118 6.13943L21 0L31.4882 6.13943Z"
        //                 fill="white"
        //             />
        //             <path
        //                 d="M10.5118 6.13943V18.4422L21 24.5816V12.2789L10.5118 6.13943Z"
        //                 fill="#DBD2E5"
        //             />
        //             <path
        //                 d="M31.4882 6.13943V18.4422L21 24.5816V12.2789L31.4882 6.13943Z"
        //                 fill="#B4A1C8"
        //             />
        //         </svg>
        //     </div>
        //     <nav className={styles.nav}>
        //         <SideBarLink
        //             title={"Dashboard"}
        //             icon={
        //                 <svg
        //                     width="14"
        //                     height="14"
        //                     viewBox="0 0 14 14"
        //                     fill="none"
        //                     xmlns="http://www.w3.org/2000/svg"
        //                 >
        //                     <path
        //                         d="M2 0C0.895431 0 0 0.89543 0 2V4C0 5.10457 0.89543 6 2 6H4C5.10457 6 6 5.10457 6 4V2C6 0.895431 5.10457 0 4 0H2ZM2 8C0.895431 8 0 8.89543 0 10V12C0 13.1046 0.89543 14 2 14H4C5.10457 14 6 13.1046 6 12V10C6 8.89543 5.10457 8 4 8H2ZM8 2C8 0.89543 8.89543 0 10 0H12C13.1046 0 14 0.895431 14 2V4C14 5.10457 13.1046 6 12 6H10C8.89543 6 8 5.10457 8 4V2ZM10 8C8.89543 8 8 8.89543 8 10V12C8 13.1046 8.89543 14 10 14H12C13.1046 14 14 13.1046 14 12V10C14 8.89543 13.1046 8 12 8H10Z"
        //                         fill="#EAE6F2"
        //                     />
        //                 </svg>
        //             }
        //             path="/dashboard"
        //         />
        //         <div className={styles.nav__divider}></div>
        //         {sideBarData.map((link) => (
        //             <SideBarLink
        //                 key={link.title}
        //                 title={link.title}
        //                 path={link.path}
        //                 icon={link.icon}
        //             />
        //         ))}
        //     </nav>

        //     <button onClick={handleClick} className={styles.logout}>
        //         <div className={styles.icon}>
        //             <svg
        //                 width="18"
        //                 height="18"
        //                 viewBox="0 0 18 18"
        //                 fill="none"
        //                 xmlns="http://www.w3.org/2000/svg"
        //             >
        //                 <path
        //                     d="M12.6 1.5H10.65C8.25 1.5 6.75 3 6.75 5.4V8.4375H11.4375C11.745 8.4375 12 8.6925 12 9C12 9.3075 11.745 9.5625 11.4375 9.5625H6.75V12.6C6.75 15 8.25 16.5 10.65 16.5H12.5925C14.9925 16.5 16.4925 15 16.4925 12.6V5.4C16.5 3 15 1.5 12.6 1.5Z"
        //                     fill="white"
        //                 />
        //                 <path
        //                     d="M3.42002 8.43738L4.97252 6.88488C5.08502 6.77238 5.13752 6.62988 5.13752 6.48738C5.13752 6.34488 5.08502 6.19488 4.97252 6.08988C4.75502 5.87238 4.39502 5.87238 4.17752 6.08988L1.66502 8.60238C1.44752 8.81988 1.44752 9.17988 1.66502 9.39738L4.17752 11.9099C4.39502 12.1274 4.75502 12.1274 4.97252 11.9099C5.19002 11.6924 5.19002 11.3324 4.97252 11.1149L3.42002 9.56238H6.75002V8.43738H3.42002Z"
        //                     fill="white"
        //                 />
        //             </svg>
        //         </div>
        //         <span>Log out</span>
        //     </button>
        // </div>
    );
};

export default SideBar;
