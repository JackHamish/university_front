import { ChangeEvent } from "react";

import styles from "./checkBox.module.scss";

interface CheckBoxProps {
    label: string;
    checked?: boolean;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}

const CheckBox = ({ label, checked, onChange }: CheckBoxProps) => {
    return (
        <div className={styles.root}>
            <input type="checkbox" checked={checked} onChange={onChange} />
            <label htmlFor={label}>{label}</label>
        </div>
    );
};

export default CheckBox;
