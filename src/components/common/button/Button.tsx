import styles from "./Button.module.scss";

interface ButtonProps {
    label: string;
    onClick?: () => void;
    type?: "submit";
}

const Button = ({ label, onClick, type }: ButtonProps) => {
    return (
        <button type={type} className={styles.root} onClick={onClick}>
            {label}
        </button>
    );
};

export default Button;
