import styles from "./Input.module.scss";
import { UseFormRegisterReturn } from "react-hook-form";

interface InputProps {
    type: "text" | "number" | "email" | "password";
    label: string;
    name: string;
    placeholder?: string;
    disabled?: boolean;
    register?: Partial<UseFormRegisterReturn>;
}

export const Input = ({
    type,
    label,
    name,
    register,
    placeholder,
    disabled,
    ...props
}: InputProps): JSX.Element => {
    return (
        <div className={styles.root}>
            <label htmlFor={label}>{label}</label>
            <input
                type={type}
                id={label}
                placeholder={placeholder}
                disabled={disabled}
                {...register}
                {...props}
            />
        </div>
    );
};

export default Input;
