import { apiCaller } from "../utils/api-caller";
import { AxiosResponse } from "axios";
import { IBaseEntity } from "./types";

export interface IGroup extends IBaseEntity {
    name: string;
}

export interface IGroupCreate extends Omit<IGroup, "id" | "createdAt" | "updatedAt"> {}

export const getGroupsApi = async (params: {
    name?: string;
    sortField?: string;
    sortOrder?: string;
    limit?: number;
    offset?: number;
}): Promise<AxiosResponse> =>
    apiCaller({
        method: "GET",
        url: "groups",
        params,
    });

export const createGroupApi = async (data: IGroupCreate): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "groups",
        data,
    });

export const updateGroupApi = async (
    id: string,
    data: Partial<IGroupCreate>
): Promise<AxiosResponse> =>
    apiCaller({
        method: "PATCH",
        url: `groups/${id}`,
        data,
    });
