import { AxiosResponse } from "axios";
import { apiCaller } from "../utils/api-caller";

export const getMeApi = async (): Promise<AxiosResponse> =>
    apiCaller({
        method: "Get",
        url: "me",
    });
