import { apiCaller } from "../utils/api-caller";
import { AxiosResponse } from "axios";
import { IBaseEntity } from "./types";

export interface IStudent extends IBaseEntity {
    name: string;
    surname: string;
    email: string;
    age: number;
    imagePath: string;
    groupId: string;
    // [key: string]: string;
}

export interface IStudentCreate
    extends Omit<IStudent, "id" | "createdAt" | "updatedAt" | "imagePath" | "groupId"> {}

export interface IStudentUpdate extends Partial<IStudentCreate> {
    groupId?: number;
    imagePath?: string;
}

export const getStudentsApi = async (params: {
    name?: string;
    sortField?: string;
    sortOrder?: string;
    limit?: number;
    offset?: number;
}): Promise<AxiosResponse> =>
    apiCaller({
        method: "GET",
        url: "students",
        params,
    });

export const getStudentByIdApi = async (id: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "GET",
        url: `students/${id}`,
    });

export const createStudentApi = async (data: IStudentCreate): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "students",
        data,
    });

export const updateStudentApi = async (id: string, data: IStudentUpdate): Promise<AxiosResponse> =>
    apiCaller({
        method: "PATCH",
        url: `students/${id}`,
        data,
    });
