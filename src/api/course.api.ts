import { apiCaller } from "../utils/api-caller";
import { AxiosResponse } from "axios";
import { IBaseEntity } from "./types";

export interface ICourse extends IBaseEntity {
    name: string;
    description: string;
    hours: number;
    studentsCount: number;
}

export interface ICourseCreate
    extends Omit<ICourse, "id" | "createdAt" | "updatedAt" | "studentsCount"> {}

export const getCoursesApi = async (params: {
    name?: string;
    sortField?: string;
    sortOrder?: string;
    limit?: number;
    offset?: number;
}): Promise<AxiosResponse> =>
    apiCaller({
        method: "GET",
        url: "courses",
        params,
    });

export const createCourseApi = async (data: ICourseCreate): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "courses",
        data,
    });

export const updateCourseApi = async (
    id: string,
    data: Partial<ICourseCreate>
): Promise<AxiosResponse> =>
    apiCaller({
        method: "PATCH",
        url: `courses/${id}`,
        data,
    });
