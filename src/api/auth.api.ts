import { apiCaller } from "../utils/api-caller";
import { AxiosResponse } from "axios";

export const registerApi = async (email: string, password: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "auth/sign-up",
        data: {
            email,
            password,
        },
    });

export const loginApi = async (email: string, password: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "auth/sign-in",
        data: {
            email,
            password,
        },
    });

export const forgotPasswordApi = async (email: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "auth/reset-password-request",
        data: {
            email,
        },
    });

export const resetPasswordApi = async (token: string, password: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "auth/reset-password",
        data: {
            token,
            newPassword: password,
        },
    });
