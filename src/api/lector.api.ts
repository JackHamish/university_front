import { apiCaller } from "../utils/api-caller";
import { AxiosResponse } from "axios";
import { IBaseEntity } from "./types";

export interface ILector extends IBaseEntity {
    name: string;
    surname: string;
    email: string;
    password: string;
    // [key: string]: string;
}

export interface ILectorCreate extends Omit<ILector, "id" | "createdAt" | "updatedAt"> {}

export const getLectorsApi = async (params: {
    name?: string;
    sortField?: string;
    sortOrder?: string;
    limit?: number;
    offset?: number;
}): Promise<AxiosResponse> =>
    apiCaller({
        method: "GET",
        url: "lectors",
        params,
    });

export const createLectorApi = async (data: ILectorCreate): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "lectors",
        data,
    });

export const updateLectorApi = async (
    id: string,
    data: Partial<ILectorCreate>
): Promise<AxiosResponse> =>
    apiCaller({
        method: "PATCH",
        url: `lectors/${id}`,
        data,
    });
