export const isNonEmpty = (value: string | number) => {
    if (typeof value === "string") {
        return value.trim() !== "";
    } else if (typeof value === "number") {
        return value !== 0;
    }
    return true;
};
